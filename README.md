# Orleans based Social Service

# Planning todo's:


### Technologies
**Item**|**Links**
-----|-----
SignalR|[GPSTacker Overview](https://dotnet.github.io/orleans/Documentation/Samples-Overview/GPS-Tracker.html); [GPSTracker on Git](https://github.com/dotnet/orleans/tree/master/Samples/GPSTracker)
MongoDB|[Storage Providers](https://dotnet.github.io/orleans/Documentation/Samples-Overview/Storage-Providers.html?q=Mongo)

### Architecture
**Item**|**Links**
-----|-----
Observers
Layers (Facades)
DI

### Flow
**Item**|**Links**
-----|-----
Console clients|[Chirper Overview](https://dotnet.github.io/orleans/Documentation/Samples-Overview/Chirper.html); [Chirper on Git](https://github.com/dotnet/orleans/tree/master/Samples/Chirper)
Web client|[TicTacToe Overview](https://dotnet.github.io/orleans/Documentation/Samples-Overview/Tic-Tac-Toe.html); [TicTacToe on Git](https://github.com/dotnet/orleans/tree/master/Samples/TicTacToe); [GPSTacker Overview](https://dotnet.github.io/orleans/Documentation/Samples-Overview/GPS-Tracker.html); [GPSTracker on Git](https://github.com/dotnet/orleans/tree/master/Samples/GPSTracker)
Simulation
Tests|[Unit Testing Grains](https://dotnet.github.io/orleans/Tutorials/Unit-Testing-Grains.html)

### Scalability
**Item**|**Links**
-----|-----
Multi-Cluster support | [Multi-Cluster Support](https://dotnet.github.io/orleans/Documentation/Multi-Cluster/Overview.html)

### See also

[Orleans Samples Overview](https://dotnet.github.io/orleans/Documentation/Samples-Overview/index.html)

[Orleans Samples on Git](https://github.com/dotnet/orleans/tree/master/Samples)

[Orleans Tutorials](https://dotnet.github.io/orleans/Tutorials/index.html)


# Tips and best practices

## Create projects

Dev/Test host project [Host]

Grain Class Collection [GrainsCollection] (is referenced by Host) 

Interface Collection [GrainInterfaces] (is referenced by both projects from above) 

Build solution (nuget packages will be installed)

### Choose key types
Guids, strings and various compound keys

compound key example:

Inherit interface from `IGrainWithGuidCompoundKey` or `IGrainWithIntegerCompoundKey`

In client code (adds a 2nd srg): `var grain = GrainClient.GrainFactory.GetGrain<IExample>(0, "a string!");`

Access the compound key: 

`string keyExtension;
long primaryKey = this.GetPrimaryKey(out keyExtension);`

All methods of communication interfaces must return Type/Type<T> (where T is serializable)

*Note: In a realistic production environment, the grain code would be deployed in a silo hosted by Windows Azure or Windows Server and the client would most likely be a Web site or service using Orleans for the backend logic*

### Configs
#### Host - App.config
##### GC

`<gcServer enabled="true"/>
<gcConcurrent enabled="false"/>`

[More on GC Config](https://dotnet.github.io/orleans/Documentation/Advanced-Concepts/Configuring-.NET-Garbage-Collection.html?q=gcServer)

#### Host – OrleansConfiguration.xml

Within the Host project, add **OrleansConfiguration.xml** (it is for cluster configuration)
Set the **Copy to Output directory** file property to **Copy if newer**:

```xml
<?xml version="1.0" encoding="utf-8"?>
<OrleansConfiguration xmlns="urn:orleans">
  <Globals>
    <SeedNode Address="localhost" Port="11111" />
  </Globals>
  <Defaults>
    <Networking Address="localhost" Port="11111" />
    <ProxyingGateway Address="localhost" Port="30000" />
  </Defaults>
</OrleansConfiguration>
```

It is possible to configure this within the code

[More on Cluster Config](https://dotnet.github.io/orleans/Documentation/Multi-Cluster/SiloConfiguration.html)

#### Host – Program.cs

## Running in a Stand-Alone Silo
Modify the solution and make it just a bit more like a production environment, running the server and client code in different processes.

Get the grain code running in a silo that is in a separate process and prevent it from being started by the code in Program.cs.

```cs
static void Main(string[] args)
{
  Console.WriteLine("Waiting for Orleans Silo to start. Press Enter to proceed...");
  Console.ReadLine();
  // Orleans comes with a rich XML and programmatic configuration.
  // Here we're just going to set up with basic programmatic config
  var config = Orleans.Runtime.Configuration.ClientConfiguration.LocalhostSilo(30000);
  GrainClient.Initialize(config);
}
```

Add reference to Microsoft.Orleans.Server NuGet package to **GrainsCollection** and then in its project properties in Debug tab set the bin/Debug/OrleansHost.exe or bin/Release/OrleansHost.exe file as startup program for your collections class library.

*Note: Not mix NuGet package versions, make sure all Orlean packages (client, graininterface and grain projects) are aligned in the solution.

OrleansHost.exe is a ready-made host executable intended for running Orleans silo code. It is also useful for development purposes. If you set both the grain collection project and the host project as startup projects, you will see two windows come up.

This allows us to debug the grains in their own process, while keeping the client code in its own process. If you let the client make its request, then terminate it using 'Enter' when asked, you should see only the client process windows disappear. The debugging session won't end, because the grains are still being debugged in the OrleansHost.exe process. To start the client again, you will have to use the right-button context menu in the Solution Explorer to get it started.


[More on Project Init](https://dotnet.github.io/orleans/Tutorials/Minimal-Orleans-Application.html)

[Orleans Best Practices](https://www.microsoft.com/en-us/research/publication/orleans-best-practices/?from=http%3A%2F%2Fresearch.microsoft.com%2Fpubs%2F244727%2Forleans%2520best%2520practices.pdf)

It is often possible to use traditional object-oriented design methodology with Orleans, but sometimes there are reasons for not doing so (like not using the inheritance a Manager is clearly also an Employe).

```cs
public interface IEmployee : IGrainWithGuidKey
{
  Task<int> GetLevel();
  Task Promote(int newLevel);
  Task<IManager> GetManager();
  Task SetManager(IManager manager);
}
public interface IManager : IGrainWithGuidKey
{
  Task<IEmployee> AsEmployee();
  Task<List<IEmployee>> GetDirectReports();
  Task AddDirectReport(IEmployee employee);
}
```
## Mesages
Messages are simply data passed from one actor to another.
When Orleans sends a message from one grain to another, it creates a **deep copy** of the object, and provides the copy to the second grain, and not the object stored in the first grain. This prohibits the mutation of state from one grain to another, one of the main tenets in the actor model is that state shouldn't be shared, and message passing is the only mechanism for exchanging data.
When the grains are in different silos, the object model is serialized to a binary format, and sent over the wire.
However, this deep copy process is **expensive**, and if you promise not to modify the message, then for communication with grains within a silo, it's unnecessary.
If you indicate to Orleans that you are not going to modify the object (i.e. it's immutable) then it can skip the deep copy step, and it will pass the object by reference. There's no way Orleans or C# can stop you from modifying the state, you have to be disciplined. This is just a signal to give to Orleans to tell it you're not going to modify this object.
Immutability is indicated with a the **`[Immutable]`** attribute on the class:
```cs
[Immutable]
public class GreetingData
{
    public Guid From { get; set; }
    public string Message { get; set; }
    public int Count { get; set; }
}
```

[Employee Manager example](https://dotnet.github.io/orleans/Tutorials/A-Service-is-a-Collection-of-Communicating-Actors.html)

[Take a look](https://dotnet.github.io/orleans/Tutorials/A-Service-is-a-Collection-of-Communicating-Actors.html#asynchrony-bites-us)


## Actor lifecycle
Grains are never created or destroyed, just moved from the inactive to the active state or vice versa.
Therefore, the implementation should catch the activation event (a call to `OnActivateAsync()`) and perform any initialization steps necessary there. It is guaranteed to be called **before any method** on the grain instance is called. In the Manager grain above, it was used to establish the reference to the Employee grain.
```cs
public class Manager : Grain, IManager
{
  public override Task OnActivateAsync()
  {
     _me = this.GrainFactory.GetGrain<IEmployee>(this.GetPrimaryKey());
    return base.OnActivateAsync();
  }
  …
}
```
There is also an `OnDeactivateAsync()` method, used more infrequently.
## Concurrency
`[Reentrant]`, means that additional calls may be made while the grain is waiting for a task to complete, resulting in interleaved execution.

```cs
async Task UpdatePrice(object stock)
{
    // collect the task variables without awaiting
    var priceTask = GetPriceFromYahoo(stock as string);
    var graphDataTask = GetYahooGraphData(stock as string);
    // await both tasks
    await Task.WhenAll(priceTask, graphDataTask);
    // read the results
    price = priceTask.Result;
    graphData = graphDataTask.Result;
    Console.WriteLine(price);
}
```
## Declarative persistence
It is sometimes the case that some of the state you are accumulating belongs in some form of permanent storage, so that it can survive a silo shutdown, or a grain migrating from one silo to another for load-balancing or a complete restart/shutdown of the service. What we have seen so far will not support such situations.

See also [Custom Storage Provider](https://dotnet.github.io/orleans/Tutorials/Custom-Storage-Providers.html)

[More](https://dotnet.github.io/orleans/Tutorials/Declarative-Persistence.html)


## Handling Failures
[Here](https://dotnet.github.io/orleans/Tutorials/Failure-Handling.html)

## Front ends for Orleans

1. Add a new ASP.NET Web Application to your solution
1. Add a reference to the Orleans.dll file in the project references
1. Init Orleans in the Global.asax.cs
```cs
namespace WebApplication1
{
  public class WebApiApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
    ...
    var config = ClientConfiguration.LocalhostSilo();
    // Attempt to connect a few times to overcome transient failures and to give the silo enough 
    // time to start up when starting at the same time as the client (useful when deploying or during development).
    const int initializeAttemptsBeforeFailing = 5;
    int attempt = 0;
    while (true)
    {
      try
      {
        GrainClient.Initialize(config);
        break;
      }
      catch (SiloUnavailableException e)
      {
        attempt++;
        if (attempt >= initializeAttemptsBeforeFailing)
        {
          throw;
        }
        Thread.Sleep(TimeSpan.FromSeconds(2));
      }
    }
    ...
```

4. Creating the Controller (Web API 2 Controller - Empty)
5. Add some Get method

```cs
public class EmployeeController : ApiController
{
  public Task<int> Get(Guid id)
 {
    var employee = GrainClient.GrainFactory.GetGrain<IEmployee>(id);
    return employee.GetLevel();
  }
}
```
Note that the controller is asynchronous, and we can just pass back the Task which the grain returns.
6. Build the project. Set the ASP.NET application and the silo host project as the startup projects, and run them.
	
	
## Deployment
[Cloud](https://dotnet.github.io/orleans/Tutorials/Cloud-Deployment.html)
[On-Premise](https://dotnet.github.io/orleans/Tutorials/On-Premise-Deployment.html)

## Unit testing
Two main ways: Using `Microsoft.Orleans.TestingHost`; or Using Moq